import  { SocketGameProcessServices }  from '../socketClientClasses/SocketGameProcessServices.mjs';

import { addClass, removeClass } from '../domHelpers/domHelper.mjs';
import {  hideElement, showElement, clearRootContainer, renderPlayer, renderRoom } from '../domHelpers/domServise.mjs';
import * as domId from '../domHelpers/domElementsId.mjs';

export class SocketRoomService {
    constructor(io, socket, username) {
        this._socket = socket;
        this._username = username; 
    }

    get username(){
        return this._username;
    }
    set username(username){
        this._username = username;
    }

    get socket() {
        return this._socket;
    }
    set socket(socket) {
        this._socket = socket;
    }

    addRoomOnPage(room) {
        if(!room.timer) // add here check count of users
          renderRoom(room, () => {
                this.socket.emit('join-room', room);
          });
    }


    displayAvailableRoom(rooms, onJoinRoom) {
        clearRootContainer(domId.roomsListContainerId);
        const roomList = document.getElementById(domId.roomsListContainerId);
      
        rooms.forEach(roomElement => {
            if (!roomElement.timer) {
                const room = renderRoom(roomElement, roomName => onJoinRoom(roomName));
                roomList.appendChild(room);
            }
        });
    }

    updateUsersInRoom(room) {
        clearRootContainer(domId.playersListContainerId);
        this.updateUserListInRoom(room.users);
        if(room.timer) {
            const socketGameProcess = new SocketGameProcessServices(this.socket, sessionStorage.getItem("username"));
            socketGameProcess.updateProgressBar(room.users);
        }

    }

    updateUserListInRoom(userList) {
        const playersList = document.getElementById(domId.playersListContainerId);
        // if user start a game lets name him a player
        userList.forEach(user => {
          const player = renderPlayer(user.username);
          playersList.appendChild(player);
      
          if(user.username === sessionStorage.getItem("username")) 
            this.markMeAtUserList(user.username);
      
          const statusItemId = `player-${user.username}-flag`;
          if (user.readyStatus)
            addClass(document.getElementById(statusItemId), 'ready');
          else 
            addClass(document.getElementById(statusItemId), 'not-ready');
        });
    }

    markMeAtUserList(username) {
        const userItemId = `player-${username}-name`;
        addClass(document.getElementById(userItemId), 'my-player-name');
    }

    openRoom(room) {
        hideElement(domId.roomsPageContainerId);
        showElement(domId.gamePageContainerId);
        document.getElementById('room-name').innerHTML = room.roomName;
    }
}
