import * as config from '../../javascript/config.mjs';
import { addClass, removeClass } from  '../domHelpers/domHelper.mjs';
import {  hideElement, showElement } from '../domHelpers/domServise.mjs';
import * as domId from '../domHelpers/domElementsId.mjs';
import { get } from '../requestHelper.mjs';

export class SocketGameProcessServices {

    constructor(socket, username) {
      this._socket = socket;
      this._username = username; 
      this._lengthTypedText = 0;
      this._countTypedError = 0;
    }

    get textGame(){
      return this._textGame;
    }
    set textGame(text){
      this._textGame = text;
    }
    get lengthTypedText(){
      return this._lengthTypedText;
    }
    set lengthTypedText(count){
      this._lengthTypedText = count;
    }
    get countTypedError(){
      return this._countTypedError;
    }
    set countTypedError(text){
      this._countTypedError = text;
    }

    runTimer = domTimerId => {
      return new Promise((resolve) => {
          let timer = setInterval(function(){
            let currentSecond = parseInt(document.getElementById(domTimerId).innerHTML);
            document.getElementById(domTimerId).innerHTML = --currentSecond;
            if (currentSecond == 0){
              clearInterval(timer);
              resolve();
            }
          }, 1000)
      });
    }

    async runStartTimer(idText) {
      console.log("before");
      const textDataRequest = await get('http://localhost:3002/game/texts', idText);
      const textValue = textDataRequest.textGame;
      this.textGame = textValue;

      console.log(this.textGame);

      hideElement(domId.buttonBackRoomsId);
      hideElement(domId.buttonReadyId);
      showElement(domId.timerId);
      
      document.getElementById(domId.timerId).innerHTML = config.SECONDS_TIMER_BEFORE_START_GAME;
      document.getElementById(domId.gameTimerValueId).innerHTML = config.SECONDS_FOR_GAME;

      /*const timerCompleted = new Promise((resolve) => {
          let timer = setInterval(function(){
            let currentSecond = parseInt(document.getElementById(runStartTimer).innerHTML);
            document.getElementById(domId.timerId).innerHTML = --currentSecond;
            if (currentSecond == 0){
              clearInterval(timer);
              resolve();
            }
          }, 1000)
        }); */
      this.runTimer(domId.timerId)
      .then(() => {
          document.getElementById(domId.textGameContainerId).innerHTML = this.textGame;
          hideElement(domId.timerId);
          showElement(domId.paragraphTextId);
          showElement(domId.gameTimerId);
          document.addEventListener('keydown', this.getKeysHandler.bind(this));
      })
      .then( () => {
        this.runTimer(domId.gameTimerValueId)
        .then(() => {
          alert('timer completed!');
        });
      })
    }

    getKeysHandler(e) {
        let key = e.key;
        const persentTyped = ((this.lengthTypedText+1)*100) / (this.textGame.length);
        const userStatusBarId = `player-${sessionStorage.getItem("username")}-statusbar-indicator`;
      
        const keyCode = key.charCodeAt(0);
        if (keyCode >= 32 && keyCode <= 126) {
          let coloredText, underlinedText, notColoredText;

          if (this.textGame[this.lengthTypedText] == key) {
            this._socket.emit('update-progress', persentTyped);
      
            coloredText = this.textGame.substring(0, this.lengthTypedText + 1);
            if(this.lengthTypedText - 1 < this.textGame.length ) {
              if(this.lengthTypedText <= this.textGame.length) {
                underlinedText = this.textGame[this.lengthTypedText + 1];
                if(underlinedText)
                  document.getElementById(domId.underlinedTextId).innerHTML = underlinedText;
                  else document.getElementById(domId.underlinedTextId).innerHTML = "";
              }
              else document.getElementById(domId.underlinedTextId).innerHTML = "";
      
              notColoredText = this.textGame.substring(this.lengthTypedText + 2, this.textGame.length);
              document.getElementById(domId.textGameId).innerHTML = notColoredText;
            }
      
            document.getElementById(domId.coloredTextId).innerHTML = coloredText;
      
            if (this.lengthTypedText + 1 === this.textGame.length) {
              document.getElementById(userStatusBarId).style.width = "100%";
              this._socket.emit('winner', this.countTypedError);
            }
            this.lengthTypedText++;
            //sessionStorage.setItem("lengthTypedText", lengthTypedText);
          } else {
            this.countTypedError++;
            //sessionStorage.setItem("countTypedError", countTypedError);
          }
        }
    }

    showGameResult(usersList) {
        let textInfo = '';
        usersList.sort(this.sortByPlace).forEach(user => {
          textInfo += `${user.username} has finished on ${user.place} place with ${user.countErrors} errors in typing; `;
        });
        this.showWindowResult(textInfo);
        this.updateGameforNextUsing(usersList);
    }

    sortByPlace(first, second) {
        if (first.place < second.place ){
          return -1;
        }
        if (first.place > second.place ){
          return 1;
        }
        return 0;
    }

    showWindowResult(textMessage){
      alert(textMessage);
    }

    updateGameforNextUsing(usersList){
        showElement(domId.buttonBackRoomsId);
        showElement(domId.buttonReadyId);
        hideElement(domId.paragraphTextId);
        hideElement(domId.timerGameId);
        document.getElementById(domId.textGameId).innerHTML = '';
        document.getElementById(domId.coloredTextId).innerHTML = '';
        document.getElementById(domId.underlinedTextId).innerHTML = '';
        document.getElementById(domId.buttonReadyId).value = "Ready";

        document.removeEventListener('keydown', this.getKeysHandler);
        this.resetUserProgressBars(usersList);

        this._socket.on('complete-game');
    }

    resetUserProgressBars(usersList) {
        let statusItemId, userStatusBarId;
        usersList.forEach(user => {
            statusItemId = `player-${user.username}-flag`;
            removeClass(document.getElementById(statusItemId), 'ready');
            addClass(document.getElementById(statusItemId), 'not-ready');

            userStatusBarId = `player-${user.username}-statusbar-indicator`;
            removeClass(document.getElementById(userStatusBarId), 'completed');
            document.getElementById(userStatusBarId).style.width = "0%";
        });
    }

    updateProgressBarToCompleted(usersInRoom) {
        const currentUser = usersInRoom.find(user => user.username === sessionStorage.getItem("username"));
        console.log(currentUser);
        if(currentUser.completedStatus)
          addClass(document.getElementById(`player-${currentUser.username}-statusbar-indicator`), 'completed');
    }

    updateProgressBar(usersList) {
        let userId;
        usersList.forEach(user => { 
          console.log(user+" in update progress bar "+user.progressBarWidth);
          if (user.progressBarWidth) {
            userId = `player-${user.username}-statusbar-indicator`;
            console.log("update "+userId+"");
            document.getElementById(userId).style.width = user.progressBarWidth + "%";
            if (user.progressBarWidth === 100)
                addClass(document.getElementById(userId), 'completed');
          }
        });
    }
}

