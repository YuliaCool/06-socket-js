
export class SocketClientParent {
    constructor(io, socket, username) {
        this._io = io;
        this._socket = socket;
        this._username = username;
    }

    get username(){
        return this._username;
    }
    set username(username){
        this._username = username;
    }

    get socket() {
        return this._socket;
    }
    set socket(socket) {
        this._socket = socket;
    }
}
