export const get = async (entityName, id) => {
    return await makeRequest(`${entityName}/${id}`, 'GET');
}

const makeRequest = async (path, method) => {
    try {
        const url = `${path}`
        const res = await fetch(url, {
            method,
            headers: { "Content-Type": "application/json" }
        }).then((response) => response.json());

        //const dataObj = await res.json();
        return res;
    } catch (err) {
        return err;
    }
}