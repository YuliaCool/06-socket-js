export const MAXIMUM_USERS_FOR_ONE_ROOM = 5;
export const SECONDS_TIMER_BEFORE_START_GAME = 4; // for test 4, for using 10
export const SECONDS_FOR_GAME = 10; // for test 10, for using 60
