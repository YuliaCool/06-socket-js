import { Router } from "express";
import path from "path";
import { HTML_FILES_PATH } from "../config";
import { texts } from "../data";

const router = Router();

router.get("/", (req, res) => {
    const page = path.join(HTML_FILES_PATH, "game.html");
    res.sendFile(page);
  });

router.get("/texts/:id", (req, res) => {
  try {
    console.log(texts[req.params.id]);
    res.send({ textGame: texts[req.params.id] });
  } catch (err) {
    res.status(400).json({
      error: true,
      message: err.message
    });
  }
});

export default router;
