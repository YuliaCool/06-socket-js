import { rooms } from './rooms'; //delete roomName from this obj, make set not map
import { users } from './users';
import { SocketParent } from './socketParent';
import { SocketGameProcess } from './socketGameProcess';
import { checkReadyStartTimer, checkReadyFinish } from './socketService';


export class SocketRooms extends SocketParent {
    constructor(io, socket) {
        super(io, socket);
    }

    createNewRoom(roomName) {
        const username = this._socket.handshake.query.username;

        if(rooms.has(roomName)){
            this._socket.emit('room-error', { message: "Room with this name has already exist!" });
        } else {
            const room = { roomName, users: [ { username, readyStatus: false } ], timer: false };
            this.joinRoom(room);
        }
    }

    addUserToRoom(roomName) {
        const username = this._socket.handshake.query.username;
        console.log(rooms);
        const usersInRoom = rooms.get(roomName).users;
        usersInRoom.push({ username, readyStatus: false } );
        const room = { roomName, users: usersInRoom, timer: false };
        this.joinRoom(room);
    }

    joinRoom(room) {
        rooms.set(room.roomName, room);
        const username = this._socket.handshake.query.username;
        users.set(username, room.roomName);

        this._socket.join(room.roomName, () => {
            this._io.to(this._socket.id).emit("open-room", room);
            this._io.to(room.roomName).emit('update-users-in-room', room);
            this._socket.broadcast.emit("display-available-rooms", Array.from(rooms.values()));
        });
    }

    leaveCurrentRoom() {
        const username = this._socket.handshake.query.username;
        const roomName = users.get(username);

        users.set(username, null);
        console.log(rooms);

        const usersInRoom = rooms.get(roomName).users;
        const gameStarted = rooms.get(roomName).timer;
        const updatedUserListInRoom = usersInRoom.filter(user => user.username !== username);

        let updatedRoom = { roomName, users: updatedUserListInRoom, timer: gameStarted };

        this.updateRoomsAfterLeaving(updatedRoom);
    }

    disconnect() {
        // get information from which room user was disconnect(if he was connected to Room), but userMap has already updated in users logout
        // update users' list of disconnect room and room's list 
        const username = this._socket.handshake.query.username;
    
        let userDisconnect;
        const roomList = Array.from(rooms.values());
        for (let room of roomList) {
            userDisconnect = room.users.find(user => user.username === username);
            if(userDisconnect) {
                const roomName = room.roomName;
                console.log(rooms);
                const usersInRoom = rooms.get(roomName).users;
                const gameStarted = rooms.get(roomName).timer;
                let connectedUsers = usersInRoom.filter(user => user.username != username);
                let updatedRoom = { roomName, users: connectedUsers, timer: gameStarted };

                this.updateRoomsAfterLeaving(updatedRoom);
            }
        }
    }

    updateRoomsAfterLeaving(updatedRoom) {
        if (updatedRoom.users.length == 0) {
            rooms.delete(updatedRoom.roomName);
            this._socket.emit("display-available-rooms", Array.from(rooms.values()));
            this._socket.broadcast.emit("display-available-rooms", Array.from(rooms.values()));
        } else {   
            let allUsersIsReady = checkReadyStartTimer(updatedRoom);
            let allUsersIsFinished = checkReadyFinish(updatedRoom);
            if(allUsersIsReady && !updatedRoom.timer) { 
                //case: user leave and all other users in room is ready for starting game -> start game
                updatedRoom.timer = true;
                rooms.set(updatedRoom.roomName, updatedRoom);

                const socketGameProcess = new SocketGameProcess(this._io, this._socket);
                socketGameProcess.startGame(updatedRoom);
            } 
            if (allUsersIsFinished && !updatedRoom.completedGame) { 
                //case: user leave and all other users in room is ready for completing game, and game result hasn't shown yet 
                // -> finish game and show results
                updatedRoom.completedGame = true;
                rooms.set(updatedRoom.roomName, updatedRoom);

                this._io.in(updatedRoom.roomName).emit('show-game-result', updatedRoom.users);
            }
               
            if ((!allUsersIsReady) || (allUsersIsReady && updatedRoom.timer)) { 
                // case: user leave and all other users in room isn't ready for completing game 
                //       or game is in a process and users doesn't ready for finishing game
                // -> update lists of users for every user in current room list and rooms list 
                rooms.set(updatedRoom.roomName, updatedRoom);
                
                this._io.to(updatedRoom.roomName).emit('update-users-in-room', updatedRoom);
                this._socket.emit("display-available-rooms", Array.from(rooms.values()));
                this._socket.broadcast.emit("display-available-rooms", Array.from(rooms.values()));
            }
        }
    }
};


