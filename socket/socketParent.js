
export class SocketParent {
    constructor(io, socket) {
        this._io = io;
        this._socket = socket;
    }
}