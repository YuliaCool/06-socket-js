import { texts } from './../data';

export const checkReadyStartTimer = room => {
    let allUsersIsReady = true;
    room.users.forEach(user => {
        if (!user.readyStatus) 
            allUsersIsReady = false;
    });
    return allUsersIsReady;
};

export const checkReadyFinish = room => {
    let allUsersIsReady = true;
    room.users.forEach(user => {
        if (!user.completedStatus) 
            allUsersIsReady = false;
    });
    return allUsersIsReady;
};

export const getTextIdForGame = () => {
    const randomId = getRandomId(texts.length);
    //const textForGame = randomId;
    return randomId;
}

const getRandomId = (maxValue) => {
    return Math.floor(Math.random() * Math.floor(maxValue));
}