import { rooms } from './rooms'; //delete roomName from this obj, make set not map
import { users } from './users';
import { SocketParent } from './socketParent';
import { SocketGameRoom } from './socketGameRoom';
import { getTextIdForGame, checkReadyStartTimer } from './socketService';


export class SocketGameProcess extends SocketParent {
    constructor(io, socket) {
        super(io, socket);
    }

    startGame(room) {
        room.timer = true;
        rooms.set(room.roomName, room);

        this._io.to(room.roomName).emit('update-users-in-room', room);
        this._io.to(room.roomName).emit('run-start-timer', getTextIdForGame());

        this._socket.emit("display-available-rooms", Array.from(rooms.values()));
    }

   updateUserProgressBar(persentTyped) {
        const username = this._socket.handshake.query.username;
        const roomName = users.get(username);
        const room = rooms.get(roomName);

        const usersInRoom = room.users;
        usersInRoom.forEach(user => {
            if(user.username == username) 
                user.progressBarWidth = persentTyped;
        });
        this._io.in(roomName).emit('update-progress-bar', usersInRoom);
    }

    updateFinishedUser(countTypedError) {
        const username = this._socket.handshake.query.username;
        const roomName = users.get(username);
        const room = rooms.get(roomName);
        const usersInRoom = room.users;
        let countFinishedUsers = 0;

        usersInRoom.forEach(user => {
            if (user.completedStatus) ++countFinishedUsers;
        });

        usersInRoom.forEach(user => {
            if (user.username == username) {
                user.completedStatus = true;
                user.countErrors = countTypedError;
                user.place = ++countFinishedUsers;
                this._io.in(roomName).emit('update-progress-bar-to-finished-state', usersInRoom);
            }
        });

        if(usersInRoom.length === countFinishedUsers) {
            this._io.in(roomName).emit('show-game-result', usersInRoom);
        }   
    }

    completeGame() {
        const username = this._socket.handshake.query.username; //check this
        const roomName = users.get(username);

        const room = rooms.get(roomName);
        const usersInRoom = room.users;
        usersInRoom.forEach(user => {
            user.readyStatus = false;
            user.completedStatus = false;
            user.countErrors = 0;
            user.place = 0;
        })
    }

}